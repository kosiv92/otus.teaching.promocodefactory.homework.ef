﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly ApplicationDbContext _applicationDbContext;

        public CustomersController(IRepository<Customer> customerRepository, ApplicationDbContext applicationDbContext)
            => (_customerRepository, _applicationDbContext) = (customerRepository, applicationDbContext);


        /// <summary>
        /// Получить список всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {            
            var customers = await _customerRepository.GetAllAsync();

            var customerModelList = customers.Select(x =>
            new CustomerShortResponse()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
            }).ToList();

            return customerModelList;
        }

        /// <summary>
        /// Получить информацию о клиенте
        /// </summary>
        /// <param name="id">Уникальный номер клиента</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var preferenceResponseList = customer.Preferences.Select(x =>
            new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name,
            }
            ).ToList();

            var promocodesResponseList = customer.PromoCodes.Select(x =>
            new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToString(),
                EndDate = x.EndDate.ToString(),
                PartnerName = x.PartnerName,
            }).ToList();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PreferenceResponses = preferenceResponseList,
                PromoCodes= promocodesResponseList
            };

            return customerModel;
        }

        /// <summary>
        /// Добавить клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var id = Guid.NewGuid();
            var preferences = new List<Preference>();
            foreach (var prefId in request.PreferenceIds)
            {
                preferences.Add(await _applicationDbContext.Preferences
                    .FirstOrDefaultAsync(p => p.Id == prefId));
            }

            var newCustomer = new Customer()
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences
            };
            try
            {
                await _customerRepository.Create(newCustomer);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }
            return Ok();

        }

        /// <summary>
        /// Обновить данные клиента
        /// </summary>
        /// <param name="id">Id номер клиента</param>
        /// <param name="request">Новые данные</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var preferences = request.PreferenceIds
                .Select(pid=>_applicationDbContext.Preferences
                .Find(pid)).ToList();
            
            var customer = new Customer()
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences
            };

            await _customerRepository.Update(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();
            try
            {
                await _customerRepository.DeleteByIdAsync(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }
    }
}