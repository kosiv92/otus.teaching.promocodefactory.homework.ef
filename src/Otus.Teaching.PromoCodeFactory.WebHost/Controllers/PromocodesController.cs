﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private ApplicationDbContext _appDbContext;

        public PromocodesController(ApplicationDbContext applicationDbContext)
            => _appDbContext = applicationDbContext;

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            var promocodesList = await _appDbContext.PromoCodes.Select(x =>
            new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToString(),
                EndDate = x.EndDate.ToString(),
                PartnerName = x.PartnerName,
            }).ToListAsync();
            return promocodesList;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            if (!(Guid.Parse(request.PreferenceId) is Guid preferenceId)) return BadRequest("Incorrect value of preference Id");
            if (!(Guid.Parse(request.PartnerManagerId) is Guid managerId)) return BadRequest("Incorrect value of manager Id");


            DateTime beginDate, endDate;

            try
            {
                beginDate = DateTime.Parse(request.BeginDate);
                endDate = DateTime.Parse(request.EndDate);
            }
            catch (Exception ex) { return BadRequest(ex.Message); }


            var preference = _appDbContext.Preferences
                .Include(p => p.Customers)
                .ThenInclude(c => c.PromoCodes)
                .FirstOrDefault(p => p.Id == Guid.Parse(request.PreferenceId));

            if (preference == null) return BadRequest("Preference with specified Id does not exist");



            var promocode = new PromoCode()
            {
                Id = Guid.NewGuid(),
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                BeginDate = beginDate,
                EndDate = endDate,
                PartnerManagerId = managerId,
                PreferenceId = preferenceId
            };

            var customerPreferences = _appDbContext.CustomersPreferences.ToList();

            promocode.CustomerPromoCodes = customerPreferences
                .Where(x => x.PreferenceId == preferenceId)
                .Select(c => new CustomerPromoCode { Id = Guid.NewGuid(), CustomerId = c.CustomerId, PromoCodeId = promocode.Id })
                .ToList();

            _appDbContext.PromoCodes.Add(promocode);
            _appDbContext.SaveChangesAsync();

            return Ok();
        }
    }
}