﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly ApplicationDbContext _appDbContext;

        public PreferenceController(ApplicationDbContext appDbContext)
            => _appDbContext = appDbContext;

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        public async Task<List<PreferenceResponse>> GetPreferencesAsync()
        {
            return await _appDbContext.Preferences.Select(p =>
            new PreferenceResponse()
            {
                Id = p.Id,
                Name = p.Name,
            }
            ).ToListAsync();            
        }
    }
}
