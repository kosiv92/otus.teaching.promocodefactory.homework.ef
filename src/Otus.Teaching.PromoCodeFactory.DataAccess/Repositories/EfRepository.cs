﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository : IRepository<Employee>, IRepository<Customer>
    {
        private readonly ApplicationDbContext _dbContext;

        public EfRepository(ApplicationDbContext dbContext)
            => _dbContext = dbContext;

        #region EmployeeMethods

        async Task<IEnumerable<Employee>> IRepository<Employee>.GetAllAsync()
        {
            return await _dbContext.Employees
                .AsNoTracking()
                .ToListAsync();
        }

        async Task<Employee> IRepository<Employee>.GetByIdAsync(Guid id)
        {
            return await _dbContext.Employees
                .AsNoTracking()
                .Include(e => e.Role)
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        async Task IRepository<Employee>.DeleteByIdAsync(Guid id)
        {
            var employeeToDelete = await _dbContext.Employees
                .FirstOrDefaultAsync(e => e.Id == id);
            _dbContext.Employees.Remove(employeeToDelete);
            _dbContext.SaveChanges();
            await Task.CompletedTask;
        }

        async Task<Guid> IRepository<Employee>.Create(Employee newItem)
        {
            await _dbContext.Employees.AddAsync(newItem);
            _dbContext.SaveChanges();
            return await Task.FromResult(newItem.Id);
        }

        Task IRepository<Employee>.Update(Employee newItem)
        {
            var updatedEmployee = _dbContext.Employees
                .FirstOrDefault(x=> x.Id == newItem.Id);

            updatedEmployee.FirstName= newItem.FirstName;
            updatedEmployee.LastName= newItem.LastName;
            updatedEmployee.Email= newItem.Email;
            updatedEmployee.Role= newItem.Role;
            updatedEmployee.AppliedPromocodesCount= 0;

            return Task.FromResult(_dbContext.SaveChanges());
            
        }

        #endregion

        #region CustomerMethods

        async Task<IEnumerable<Customer>> IRepository<Customer>.GetAllAsync()
        {
            return await _dbContext.Customers
                .AsNoTracking()
                .Include(c => c.Preferences)
                .Include(c => c.PromoCodes)
                .ToArrayAsync();
        }

        async Task<Customer> IRepository<Customer>.GetByIdAsync(Guid id)
        {
            return await _dbContext.Customers
                .AsNoTracking()
                .Include(c => c.Preferences)
                .Include(c => c.PromoCodes)
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        Task IRepository<Customer>.DeleteByIdAsync(Guid id)
        {
            var customerToDelete = _dbContext.Customers
                .FirstOrDefault(c => c.Id == id);
            _dbContext.Customers.Remove(customerToDelete);
            _dbContext.SaveChanges();
            return Task.CompletedTask;
        }

        async Task<Guid> IRepository<Customer>.Create(Customer newItem)
        {
            await _dbContext.Customers.AddAsync(newItem);
            _dbContext.SaveChanges();
            return await Task.FromResult(newItem.Id);
        }

        Task IRepository<Customer>.Update(Customer newItem)
        {
            var updatedCustomer = _dbContext.Customers
                .FirstOrDefault(x => x.Id == newItem.Id);

            updatedCustomer.FirstName = newItem.FirstName;
            updatedCustomer.LastName = newItem.LastName;
            updatedCustomer.Email = newItem.Email;
            updatedCustomer.Preferences = newItem.Preferences;            

            return Task.FromResult(_dbContext.SaveChanges());

        }

        #endregion


    }

}
