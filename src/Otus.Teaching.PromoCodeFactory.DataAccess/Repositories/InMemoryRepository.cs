﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        object locker;
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
            locker = new object();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> Create(T newItem)
        {
            lock (locker)
            {
                Data = Data.Append(newItem);
            }
            return Task.FromResult(newItem.Id);
        }

        public Task Update(T newItem)
        {
            var updatedItem = Data.FirstOrDefault(x => x.Id == newItem.Id);
            var dataList = Data.ToList();
            dataList.Remove(updatedItem);
            dataList.Add(newItem);
            lock (locker)
            {
                Data = dataList;
            }
            return Task.FromResult(Data);
        }

        public Task DeleteByIdAsync(Guid id)
        {
            var deletedItem = Data.FirstOrDefault(x => x.Id == id);
            var dataList = Data.ToList();
            dataList.Remove(deletedItem);
            lock (locker)
            {
                Data = dataList;
            }
            return Task.FromResult(Data);
        }
    }
}