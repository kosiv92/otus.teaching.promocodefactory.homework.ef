﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; } = null!;

        public DbSet<Role> Roles { get; set; } = null!;

        public DbSet<Customer> Customers { get; set; } = null!;

        public DbSet<Preference> Preferences { get; set; } = null!;

        public DbSet<PromoCode> PromoCodes { get; set; } = null!;

        public DbSet<CustomerPreference> CustomersPreferences { get; set; } = null!;

        public DbSet<CustomerPromoCode> CustomersPromoCodes { get; set; } = null!;
                
        public ApplicationDbContext(DbContextOptions options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {                        
            modelBuilder.Entity<Employee>()
                .Ignore(e => e.FullName)
                .HasOne(e=>e.Role)
                .WithMany(r=>r.Employees)
                .OnDelete(DeleteBehavior.SetNull);

            #region CustomerConfiguration
                        
            modelBuilder.Entity<Customer>()
                .Ignore(c => c.FullName);

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Preferences)
                .WithMany(p => p.Customers)
                .UsingEntity<CustomerPreference>(
                j => j
                .HasOne(cp => cp.Preference)
                .WithMany(p => p.CustomerPreferences),
                j => j
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences),
                j => j
                .HasKey(cp => new { cp.PreferenceId, cp.CustomerId }));

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.PromoCodes)
                .WithMany(p => p.Customers)
                .UsingEntity<CustomerPromoCode>(
                j => j
                .HasOne(cp => cp.PromoCode)
                .WithMany(p => p.CustomerPromoCodes),
                j => j
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.CustomerPromoCodes),
                j => j
                .HasKey(cp => new { cp.PromoCodeId, cp.CustomerId }));

            #endregion

            modelBuilder.Entity<Role>()
                .HasData(FakeDataFactory.Roles);

            modelBuilder.Entity<Employee>()
                .HasData(FakeDataFactory.Employees);

            modelBuilder.Entity<Customer>()
                .HasData(FakeDataFactory.Customers);

            modelBuilder.Entity<Preference>()
                .HasData(FakeDataFactory.Preferences);
            
            modelBuilder.Entity<PromoCode>()
                .HasData(FakeDataFactory.PromoCodes);

            modelBuilder.Entity<CustomerPreference>()
                .HasData(FakeDataFactory.CustomersPreferences);

            modelBuilder.Entity<CustomerPromoCode>()
                .HasData(FakeDataFactory.CustomersPromoCodes);

        }
    }

}
