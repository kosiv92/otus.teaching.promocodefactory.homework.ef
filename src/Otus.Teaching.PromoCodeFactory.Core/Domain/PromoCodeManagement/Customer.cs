﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [Required]
        [MaxLength(30)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(30)]
        public string LastName { get; set; }
                
        public string FullName => $"{FirstName} {LastName}";

        [Required]
        [MaxLength(50)]
        public string Email { get; set; }

        public List<Preference> Preferences { get; set; }

        public List<CustomerPreference> CustomerPreferences { get; set; }

        public List<PromoCode> PromoCodes { get; set; }

        public List<CustomerPromoCode> CustomerPromoCodes { get; set; }

                    
    }
}