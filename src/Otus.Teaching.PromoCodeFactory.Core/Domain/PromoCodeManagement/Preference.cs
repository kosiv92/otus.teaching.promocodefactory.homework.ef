﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [MaxLength(30)]
        public string Name { get; set; }
                
        public List<Customer> Customers { get; set; }

        public List<CustomerPreference> CustomerPreferences { get; set; }
    }
}