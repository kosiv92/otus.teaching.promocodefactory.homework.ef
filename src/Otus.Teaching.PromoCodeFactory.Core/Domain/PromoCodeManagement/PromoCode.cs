﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [Required]
        [MaxLength(30)]
        public string Code { get; set; }

        [Required]
        [MaxLength(50)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        [MaxLength(30)]
        public string PartnerName { get; set; }

        public Guid PartnerManagerId { get; set; }

        public Employee PartnerManager { get; set; }

        public Preference Preference { get; set; }

        public Guid PreferenceId { get; set; }
                
        public List<Customer> Customers { get; set; }

        public List<CustomerPromoCode> CustomerPromoCodes { get; set; }
    }
}